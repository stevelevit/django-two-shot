from django.shortcuts import render, redirect
from django.db import models
from receipts.models import Receipt
from accounts.forms import LoginForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})